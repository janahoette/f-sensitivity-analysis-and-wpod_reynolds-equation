# f-sensitivity analysis and wpod_reynolds equation

This project contains the main functions of the code of my Bachelor's Thesis and a small scale example. 

## Summary

The purpose of aff_dec_wpod_fsens.py is to compute the solution of the Reynolds equation using finite differences. To improve the computation time, the affine parametric dependence of the matrix of the corresponding linear system was taken advantage of. Two different versions of weighted proper orthogonal decomposition can be used to reduce the system. Different variations of the wPOD can be chosen: Gauss-Legendre wPOD, Clenshaw-Curtis wPOD, Uniform Monte Carlo wPOD and Monte Carlo wPOD. The wPOD approximated solutions then are compared to the finite difference solutions. 

Three different performance parameters are computed, the maximum pressure, the load-carrying capacity and the friction force. An f-sensitivity analysis can then be carried out. 

Note that this code is not edited yet. 

## Needed packages

As the wPOD versions Clenshaw-Curtis and Gauss-Legendre are computed with sparse grids, one will need to install the Python package Tasmanian. 

## References
The main references this code is based on are the following. 

wPOD:
- D.A. Bistrian and R.F. Susan-Resiga. Weighted proper orthogonal decomposition of the swirling flow exiting the hydraulic turbine runner. Applied Mathematical Modelling, 40(5):4057–4078, 2016
-  Luca Venturi, Franscesco Ballarin, and Gianluigi Rozza. A weighted pod
method for elliptic pdes with random input. Journal of Scientific Computing,
81:136–153, 2019.

f-sensitivity index:
- Sharif Rahman. The f-sensitivity index, 2015.

