# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.tri as mtri
import numpy as np
import math
import scipy
import time
import Tasmanian
from scipy import stats
from scipy.stats import gaussian_kde
from scipy.interpolate import griddata
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from scipy.integrate import simps

from matplotlib import cm
from scipy import interpolate

from scipy.interpolate import interp1d

from matplotlib import ticker as mticker

#%%

def h(x, c, erel, d=0.08/math.pi):          #gap height function
    return c*(1+erel*math.cos((2*x)/d))
  
def hstrich(x, c, erel, d=0.08/math.pi):    #derivative of h
    return -(2*c*erel*math.sin((2*x)/d))/d

def affine_dec (Plot, Nx, Ny, xstart, xende, ystart, yende, d): #computes the affine parametric dependence high fidelity matrices
    
    xi=np.linspace(xstart, xende, Nx+1)                         #matrices used to solve the linear sytem obtained by finite differences 
    coswerte=[math.cos(2*x/d) for x in xi]                      #for the Reynolds equation
    sinwerte=[math.sin(2*x/d) for x in xi]                      #sparse matrices used 
    Hx=(xende-xstart)/Nx; Hy=(yende-ystart)/Ny
    
    
    row_ind=np.zeros((Nx+1)*2 + 2*(Ny-1))
    col_ind=np.zeros((Nx+1)*2 + 2*(Ny-1))
    data=np.zeros((Nx+1)*2 + 2*(Ny-1))
    
    l=0; m=0; n=0                                               #boundary conditions
    for i in range(Nx+1):                    #Rand oben
        row_ind[l]=i; col_ind[m]=i; data[n]=1
        l+=1; m+=1; n+=1
        
    for i in range(Nx+1):                    #Rand unten
        row_ind[l]=Ny*(Nx+1)+i; col_ind[m]=Ny*(Nx+1)+i; data[n]=1
        l+=1; m+=1; n+=1
        
    for i in range(Ny-1):                    #Rand links
        row_ind[l]=Nx+1+i*(Nx+1); col_ind[m]=Nx+1+i*(Nx+1); data[n]=1
        l+=1; m+=1; n+=1
        
    for i in range(Ny-1):                    #Rand rechts
        row_ind[l]=Nx+1+i*(Nx+1)+Nx; col_ind[m]=Nx+1+i*(Nx+1)+Nx; data[n]=1
        l+=1; m+=1; n+=1    
    
    
    row_ind=row_ind.astype(int)
    col_ind=col_ind.astype(int)
        
    A_00=scipy.sparse.coo_matrix((data, (row_ind, col_ind)))
    
    
    row_ind=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    col_ind=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    data=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    
    
    l=0; m=0; n=0
    for i in range(Nx+1):                    #Rand oben
        row_ind[l]=i; col_ind[m]=i; data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Nx+1):                    #Rand unten
        row_ind[l]=Ny*(Nx+1)+i; col_ind[m]=Ny*(Nx+1)+i; data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Ny-1):                    #Rand links
        row_ind[l]=Nx+1+i*(Nx+1); col_ind[m]=Nx+1+i*(Nx+1); data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Ny-1):                    #Rand rechts
        row_ind[l]=Nx+1+i*(Nx+1)+Nx; col_ind[m]=Nx+1+i*(Nx+1)+Nx; data[n]=0
        l+=1; m+=1; n+=1    
            
    for i in range((Nx+1)*(Ny+1)):                                                   #für Inneres Zeilen auffüllen
        if i>Nx and i<(Ny*(Nx+1)) and i % (Nx+1) != 0 and (i-Nx) % (Nx+1) != 0:       #Ränder nicht machen  
            
            row_ind[l]=i; col_ind[m]=i; data[n]=(-4)*Hy**2 - 4*Hx**2  #p(xi,yi)
            l+=1; m+=1; n+=1
          
            row_ind[l]=i; col_ind[m]=i-(Nx+1); data[n]=2*Hx**2       #p(xi,yi-1)                 
            l+=1; m+=1; n+=1
        
            row_ind[l]=i; col_ind[m]=i+Nx+1; data[n]=2*Hx**2         #p(xi,yi+1) 
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i-1; data[n]=2*Hy**2                  #p(xi-1,yi) 
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i+1; data[n]=2*Hy**2           #p(xi+1,yi)                                      
            l+=1; m+=1; n+=1
    
    row_ind=row_ind.astype(int)
    col_ind=col_ind.astype(int)
        
    A_0=scipy.sparse.coo_matrix((data, (row_ind, col_ind)))
    
    
    
    
    row_ind=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    col_ind=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    data=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    
    l=0; m=0; n=0
    
    for i in range(Nx+1):                    #Rand oben
        row_ind[l]=i; col_ind[m]=i; data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Nx+1):                    #Rand unten
        row_ind[l]=Ny*(Nx+1)+i; col_ind[m]=Ny*(Nx+1)+i; data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Ny-1):                    #Rand links
        row_ind[l]=Nx+1+i*(Nx+1); col_ind[m]=Nx+1+i*(Nx+1); data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Ny-1):                    #Rand rechts
        row_ind[l]=Nx+1+i*(Nx+1)+Nx; col_ind[m]=Nx+1+i*(Nx+1)+Nx; data[n]=0
        l+=1; m+=1; n+=1
    
    
    for i in range((Nx+1)*(Ny+1)):                                                   #für Inneres Zeilen auffüllen
        if i>Nx and i<(Ny*(Nx+1)) and i % (Nx+1) != 0 and (i-Nx) % (Nx+1) != 0:       #Ränder nicht machen  
        
            row_ind[l]=i; col_ind[m]=i; data[n]=3*coswerte[i % (Nx+1)] *((-4)*Hy**2 - 4*Hx**2)  #p(xi,yi)
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i-(Nx+1); data[n]=3*coswerte[i % (Nx+1)] *2*Hx**2             #p(xi,yi-1)                   
            l+=1; m+=1; n+=1
        
            row_ind[l]=i; col_ind[m]=i+Nx+1; data[n]=3*coswerte[i % (Nx+1)] *2*Hx**2               #p(xi,yi+1) 
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i-1; data[n]=3*coswerte[i % (Nx+1)] *2*Hy**2    + 6*Hx*Hy**2 /d * sinwerte[i % (Nx+1)]     #p(xi-1,yi)
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i+1; data[n]=3*coswerte[i % (Nx+1)] *2*Hy**2 - 6*Hx*Hy**2 /d * sinwerte[i % (Nx+1)]         #p(xi+1,yi)                                      
            l+=1; m+=1; n+=1
    
    row_ind=row_ind.astype(int)
    col_ind=col_ind.astype(int)
        
    A_erel=scipy.sparse.coo_matrix((data, (row_ind, col_ind)))
    
    
    
    
    
    row_ind=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    col_ind=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    data=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    
    l=0; m=0; n=0
    
    for i in range(Nx+1):                    #Rand oben
        row_ind[l]=i; col_ind[m]=i; data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Nx+1):                    #Rand unten
        row_ind[l]=Ny*(Nx+1)+i; col_ind[m]=Ny*(Nx+1)+i; data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Ny-1):                    #Rand links
        row_ind[l]=Nx+1+i*(Nx+1); col_ind[m]=Nx+1+i*(Nx+1); data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Ny-1):                    #Rand rechts
        row_ind[l]=Nx+1+i*(Nx+1)+Nx; col_ind[m]=Nx+1+i*(Nx+1)+Nx; data[n]=0
        l+=1; m+=1; n+=1
    
    for i in range((Nx+1)*(Ny+1)):                                                   #für Inneres Zeilen auffüllen
        if i>Nx and i<(Ny*(Nx+1)) and i % (Nx+1) != 0 and (i-Nx) % (Nx+1) != 0:       #Ränder nicht machen  
        
            row_ind[l]=i; col_ind[m]=i; data[n]=((-4)*Hy**2 - 4*Hx**2)*3*coswerte[i % (Nx+1)]**2   #p(xi,yi)
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i-(Nx+1); data[n]=2*Hx**2*3*coswerte[i % (Nx+1)]**2             #p(xi,yi-1)                     
            l+=1; m+=1; n+=1
        
            row_ind[l]=i; col_ind[m]=i+Nx+1; data[n]=2*Hx**2*3*coswerte[i % (Nx+1)]**2               #p(xi,yi+1)
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i-1; data[n]=2*Hy**2*3*coswerte[i % (Nx+1)]**2       + 6*Hx*Hy**2 /d *sinwerte[i%(Nx+1)] *2*coswerte[i%(Nx+1)]  #p(xi-1,yi)
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i+1; data[n]=2*Hy**2*3*coswerte[i % (Nx+1)]**2 -6*Hx*Hy**2 /d *sinwerte[i%(Nx+1)] *2*coswerte[i%(Nx+1)]           #p(xi+1,yi)                                      
            l+=1; m+=1; n+=1
    
    row_ind=row_ind.astype(int)
    col_ind=col_ind.astype(int)
    
    A_erel_hoch2=scipy.sparse.coo_matrix((data, (row_ind, col_ind)))
    
    
    
    row_ind=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    col_ind=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    data=np.zeros(2*Nx+2*Ny+5*(Nx-1)*(Ny-1))
    
    l=0; m=0; n=0

    for i in range(Nx+1):                    #Rand oben
        row_ind[l]=i; col_ind[m]=i; data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Nx+1):                    #Rand unten
        row_ind[l]=Ny*(Nx+1)+i; col_ind[m]=Ny*(Nx+1)+i; data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Ny-1):                    #Rand links
        row_ind[l]=Nx+1+i*(Nx+1); col_ind[m]=Nx+1+i*(Nx+1); data[n]=0
        l+=1; m+=1; n+=1
        
    for i in range(Ny-1):                    #Rand rechts
        row_ind[l]=Nx+1+i*(Nx+1)+Nx; col_ind[m]=Nx+1+i*(Nx+1)+Nx; data[n]=0
        l+=1; m+=1; n+=1
    
    for i in range((Nx+1)*(Ny+1)):                                                   #für Inneres Zeilen auffüllen
        if i>Nx and i<(Ny*(Nx+1)) and i % (Nx+1) != 0 and (i-Nx) % (Nx+1) != 0:       #Ränder nicht machen  
        
            row_ind[l]=i; col_ind[m]=i; data[n]=((-4)*Hy**2 - 4*Hx**2)*coswerte[i % (Nx+1)]**3  #p(xi,yi)
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i-(Nx+1); data[n]=2*Hx**2*coswerte[i % (Nx+1)]**3             #p(xi,yi-1)                   
            l+=1; m+=1; n+=1
        
            row_ind[l]=i; col_ind[m]=i+Nx+1; data[n]=2*Hx**2*coswerte[i % (Nx+1)]**3               #p(xi,yi+1)
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i-1; data[n]=2*Hy**2*coswerte[i % (Nx+1)]**3     + 6*Hx*Hy**2 /d *sinwerte[i%(Nx+1)]  * coswerte[i%(Nx+1)]**2      #p(xi-1,yi)
            l+=1; m+=1; n+=1
            
            row_ind[l]=i; col_ind[m]=i+1; data[n]=2*Hy**2*coswerte[i % (Nx+1)]**3   -6*Hx*Hy**2 /d *sinwerte[i%(Nx+1)]  * coswerte[i%(Nx+1)]**2       #p(xi+1,yi)                                      
            l+=1; m+=1; n+=1
    
    row_ind=row_ind.astype(int)
    col_ind=col_ind.astype(int)
    
    A_erel_hoch3=scipy.sparse.coo_matrix((data, (row_ind, col_ind)))
    
    
    buerel=[(-24)*Hx**2 * Hy**2/d *sinwerte[j] for i in range(Ny-1) for j in range(Nx+1)] 
    buerel=[0] * (Nx+1) + buerel + [0] * (Nx+1)
    
    return A_00, A_0, A_erel, A_erel_hoch2, A_erel_hoch3, buerel

def aff_dec_hd(A00, A0, A1, A2, A3, buerel, u, erel, mu , c, Nx, Ny, xstart=0, xende=0.08, ystart=0, yende=0.02,  d=0.08/math.pi):  #computes the high fidelity solution using affine parametric dependence
    A= A00 + c**3* A0 + c**3* erel * A1 + c**3* erel**2 * A2 + c**3* erel**3 * A3
    
    b=np.array(buerel)
    b= mu* c* u * erel  * b 
    tic=time.perf_counter()
    
    xy=scipy.sparse.linalg.spsolve(A , b)
    
    toc=time.perf_counter()
    #print(f"fd solution with af dec acquried in {toc - tic:0.4f} seconds")
    
    xy=np.array(xy)    
    return xy

def aff_dec_wPOD(V, A00, A0, A1, A2, A3, buerel, us, m_u, erels, cs, mus, Nx, Ny, xstart=0, xende=0.08, ystart=0, yende=0.02, d=0.08/math.pi):  #computes the (w)pod solutions and the corresponding perfomance parameters
    tic=time.perf_counter()
    AN00=V @ A00 @ np.transpose(V)
    AN0=V @ A0 @ np.transpose(V)
    AN1=V @ A1 @ np.transpose(V)
    AN2=V @ A2 @ np.transpose(V)
    AN3=V @ A3 @ np.transpose(V)
    
    buerelN=np.dot(V,np.transpose(np.mat(buerel)))
    lsg=[0]*m_u
    p_max=[0]*m_u
    F=[0]*m_u
    W=[0]*m_u
    V=np.transpose(V)
    V=np.mat(V)
    
    toc=time.perf_counter()
    
    print(f"vorarbeit acquired in {toc - tic:0.4f} seconds")
    for k in range(m_u):
        
        tic=time.perf_counter()
        AN=AN00 + cs[k]**3 * AN0 + cs[k]**3 * erels[k] *AN1 + cs[k]**3 * erels[k]**2 *AN2 + cs[k]**3 *  erels[k]**3 * AN3
        bN=  cs[k] * mus[k] * us[k] * erels[k] * np.array(buerelN)
        toc=time.perf_counter()
        
        print(f"wpod with aff dec solution acquired in {toc - tic:0.4f} seconds")
        
        
        xy=np.linalg.solve(AN,bN)
        
        
        approxuh=np.matmul(V,np.mat(xy))
        
        lsg[k]=approxuh
        
        #tic=time.perf_counter()
        p_max[k]=np.amax(approxuh)      #maximum pressure
        #toc=time.perf_counter()
        
        #print(f"max acquired in {toc - tic:0.4f} seconds")
        
         
         #load carrying capacity
        #tic=time.perf_counter()
        p_pos=approxuh.clip(min=0)
        #toc=time.perf_counter()
        y=np.linspace(0,0.08,Nx+1)
        x=np.linspace(0,0.02,Ny+1)
        
        p_pos.shape=(Ny+1, Nx+1)
        
        W[k]=simps(simps(p_pos, y), x)
        #print(f"W acquired in {toc - tic:0.4f} seconds")
        
        #tic=time.perf_counter()
        pdiff=np.zeros((Ny+1,Nx+1)) #journal force
        
        approxuh.shape=(Ny+1, Nx+1)
        for i in range(Ny+1):
            pdiff[i,:Nx]=np.diff(approxuh[i,:])/(0.08/(Nx))
        
        pdiff=pdiff[:,:Nx]
        xi=np.linspace(0,0.08,Nx+1)


        h_comp=[h(j, cs[k], erels[k], 0.08/math.pi) for j in xi]
        
        h_werte=np.tile(h_comp, (Ny+1, 1))
        h_werte=h_werte[:,:Nx]
        tau=h_werte/2 * pdiff + mus[k] *us[k]/h_werte

        F[k]=simps(simps(tau, y[:Nx]), x)
        #toc=time.perf_counter()
        
        
        #print(f"F acquired in {toc - tic:0.4f} seconds")
    return lsg, p_max, W, F



def euclidian_diff_app_error(lsg, lsgapprox, m_u,us, pltnr, title):             #computes and plots app error in euclidian norm
    
    diffv=np.zeros(m_u)
    diffv2=np.zeros(m_u)
    for  i in range(m_u):
        diffv[i]=np.linalg.norm(lsg[i]-lsgapprox[i])
        diffv2[i]=np.linalg.norm(lsg[i]-lsgapprox[i])**2
    plt.figure(pltnr, dpi=800)
    plt.xlabel(" ")
    plt.ylabel(r'$||u(\gamma)-V_Nu_N(\gamma)||$')
    plt.title(title)
    plt.semilogy(np.linspace(1,m_u, m_u), diffv, linestyle = "" , marker = ".")

    #plt.savefig(dateiname, bbox_inches='tight')
    return diffv, 1/m_u*sum(diffv2)

#%%

def sort(A):                                       
    for b in range (1, len(A[0])):
        for k in range(len(A[0])-b):
            if (np.abs(A[0][k]) > np.abs(A[0][k+1])):
                c=A[0][k]
                A[0][k]= A[0][k+1]
                A[0][k+1]=c
                
                
                A[1][:,[k, k+1]] = A[1][:,[k+1, k]]
    return A

def delcomplex(A):
    n=0; t=len(A[0])
    X=np.zeros(t); Y=np.zeros((t,t))
    
    for i in range (t):
        if ( np.imag(A[0][i]) ==0):
            X[n]=A[0][i]
            temp=A[1][:,i]
            temp.shape=(t,)
            Y[:,n]=temp
            n=n+1
    X=X[:n]
    Y=Y[:,0:(n)]
    B=tuple((X,Y))
    return B,n

#compute subspace V_N for weighted POD

def subspaceV (quadrature, method, N, Nx, Ny,t, u_train=0,  erel_train=0, mu_train=0, c_train=0 ):
    
    u_a=0.4; u_b=20; erel_a=0.1; erel_b=0.9; mu_a=0.005 ; mu_b= 0.05; c_a= 10*10**(-6);  c_b=50*10**(-6)

    mean = [10.05, 0.5, 0.0275, 30*10**(-6)]
    cov = [[1.8**2, 0, 0, 0], [0, 0.072**2, 0, 0], [0, 0,  0.004**2, 0], [0, 0, 0, 0.000003**2]] 
    param_dist=stats.multivariate_normal(mean, cov, allow_singular=True)
    
    A00, A0, A1, A2, A3, b = affine_dec(0, Nx, Ny, 0, 0.08, 0, 0.02, 0.08/math.pi)
    
    if (quadrature=="Uniform Monte-Carlo"):
        u_train, erel_train, mu_train, c_train  = np.random.uniform(u_a, u_b,t), np.random.uniform(erel_a,erel_b,t), np.random.uniform(mu_a, mu_b, t), np.random.uniform(c_a, c_b, t)         
        tic = time.perf_counter()
        B = [aff_dec_hd(A00, A0, A1, A2, A3, b, u_train[i], erel_train[i], mu_train[i], c_train[i], Nx, Ny) for i in range(t)]
        toc = time.perf_counter()
        print(f"high fidelity solutions with af dec acquried in {toc - tic:0.4f} seconds")
        
        Weights=np.zeros((t,t))
        for i in range(t):
            Weights[i,i]=param_dist.pdf([u_train[i], erel_train[i], mu_train[i], c_train[i]])/t
            
        sumweights=Weights.sum()
        
        
    if (quadrature=="Monte Carlo"):
        u_train, erel_train, mu_train, c_train = np.random.multivariate_normal(mean, cov, t).T
        
        tic = time.perf_counter()
        B = [aff_dec_hd(A00, A0, A1, A2, A3, b, u_train[i], erel_train[i], mu_train[i], c_train[i], Nx, Ny) for i in range(t)]
        toc = time.perf_counter()
        
        Weights=np.zeros((t,t))
        for i in range (t):
            Weights[i,i]=1/t
        
        sumweights=Weights.sum()
        
            
    if (quadrature=="clenshaw-curtis"):
        iNumDimensions = 4
        iLevel = 4

        grid = Tasmanian.SparseGrid()
        grid.makeGlobalGrid(iNumDimensions, 0, iLevel, "level", "clenshaw-curtis")
        aPoints = grid.getPoints()
        t=len(aPoints[:,0])
        
        u_train=(u_b-u_a)/2* aPoints[:,0]+ (u_a+u_b)/2
        erel_train=(erel_b-erel_a)/2* aPoints[:,1]+ (erel_a+erel_b)/2
        mu_train=(mu_b-mu_a)/2* aPoints[:,2]+ (mu_a+mu_b)/2
        c_train=(c_b-c_a)/2* aPoints[:,3]+ (c_a+c_b)/2
        
        B = [aff_dec_hd(A00, A0, A1, A2, A3, b, u_train[i], erel_train[i], mu_train[i], c_train[i], Nx, Ny) for i in range(t)]
        
        aWeights = grid.getQuadratureWeights()
        Weights=np.diag(aWeights)
        
        for i in range(len(aWeights)):
            Weights[i,i]=Weights[i,i]*param_dist.pdf([u_train[i], erel_train[i], mu_train[i], c_train[i]])  
            
        sumweights=Weights.sum()
        
        
    if(quadrature=="gauss-legendre"):
        iNumDimensions = 4
        iLevel = 4
        
        grid = Tasmanian.SparseGrid()
        grid.makeGlobalGrid(iNumDimensions, 0, iLevel, "level", "gauss-legendre")
        aPoints = grid.getPoints()
        t=len(aPoints[:,0])
        
        u_train=(u_b-u_a)/2* aPoints[:,0]+ (u_a+u_b)/2
        erel_train=(erel_b-erel_a)/2* aPoints[:,1]+ (erel_a+erel_b)/2
        mu_train=(mu_b-mu_a)/2* aPoints[:,2]+ (mu_a+mu_b)/2
        c_train=(c_b-c_a)/2* aPoints[:,3]+ (c_a+c_b)/2
        
        B = [aff_dec_hd(A00, A0, A1, A2, A3, b, u_train[i], erel_train[i], mu_train[i], c_train[i], Nx, Ny) for i in range(t)]
        
        
        aWeights = grid.getQuadratureWeights()
        
        Weights=np.diag(aWeights)
        
        for i in range(len(aWeights)):
            Weights[i,i]=Weights[i,i]*param_dist.pdf([u_train[i], erel_train[i], mu_train[i], c_train[i]])  
        
        sumweights=Weights.sum()
          
    WeightedMatrix=np.dot(np.dot(Weights, B), np.transpose(B))
    WM=np.dot(Weights,B)
    
    if (method=="paper"):                           #method from first paper, deleting complex eigvectors
        ev=np.linalg.eig(WeightedMatrix)
        ev=sort(ev)
        ev,n=delcomplex(ev)
        
        if (n<N):
            print ("Error to litle real eigenvalues")
            print(n)
        
        V=[0]*N
        
        for i in range(N):
            for j in range(t):
                V[i]=V[i]+ev[1][j,n-1-i]*B[j]
                
        V=np.transpose(np.mat(V))
        
    if (method=="papernotdel"):                           #method from first paper, not complex eigvectors
        ev=np.linalg.eig(WeightedMatrix)
        ev=sort(ev)
        
        n=t
        
        V=[0]*N
        
        for i in range(N):
            for j in range(t):
                V[i]=V[i]+ev[1][j,n-1-i]*B[j]
                
        V=np.transpose(np.mat(V))
        C=0
        
    if (method=="newpapereig"):                                            #Method from New Paper
        C=np.dot(WM,np.transpose(WM))
        C=C/sumweights
        ev=scipy.linalg.eigh(C, eigvals=(t-N,t-1))
        
        V=[0]*N
        
        for i in range(N):
            V[i]=(np.dot(np.transpose(WM),ev[1][:,N-1-i]))/np.linalg.norm(np.dot(np.transpose(WM),ev[1][:,N-1-i]))
        
        V=np.transpose(np.mat(V))
        
        
        
    return C, WM, B,  V, u_train, erel_train, mu_train, c_train, ev, sumweights

#%%

def interpolate2d(xmin,xmax,ymin,ymax, x_complete, y_complete, x_smaller, y_smaller, x_bigger, y_bigger):
    X, Y = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
    positions = np.vstack([X.ravel(), Y.ravel()])
    values=np.vstack([x_complete, y_complete])
    kernel = gaussian_kde(values, bw_method='silverman')
    tic=time.perf_counter()
    Z = np.reshape(kernel(positions).T, X.shape)
    toc=time.perf_counter()
    print(f"10000 kde acquried in {toc - tic:0.4f} seconds")


    #grid plotten
    
    fig = plt.figure(figsize=(13, 7))
    ax = plt.axes(projection='3d')
    w = ax.plot_wireframe(X, Y, Z)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('PDF')
    ax.set_title('Wireframe plot of Gaussian 2D KDE');

    #grid plotten zweite variante 
    
    fig = plt.figure()
    ax = fig.gca()
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    # Contourf plot
    cfset = ax.contourf(X, Y, Z, cmap='Blues')
    ## Or kernel density estimate plot instead of the contourf plot
    #ax.imshow(np.rot90(f), cmap='Blues', extent=[xmin, xmax, ymin, ymax])
    # Contour plot
    cset = ax.contour(X, Y, Z, colors='k')
    # Label plot
    ax.clabel(cset, inline=1, fontsize=10)
    ax.set_xlabel('Y1')
    ax.set_ylabel('Y0')
    
    #interpolate 


    x=np.linspace(xmin,xmax,100)
    y=np.linspace(ymin, ymax , 100 )
    ifun=scipy.interpolate.RegularGridInterpolator((x,y), Z, method='cubic')

    interpolated_data_smaller=ifun((x_smaller,y_smaller))

    positions = np.vstack([x_bigger.ravel(), y_bigger.ravel()])
    interpolated_data_bigger=kernel(positions).T
    
    interpolated_data=np.concatenate((interpolated_data_bigger, interpolated_data_smaller))
    
    return interpolated_data, X, Y, Z, positions

def completefsensanalysis(x, y, schranke, mean_x, sd_x, L=1000000):
    y_klein=y[y<=schranke]                            #unterteile Daten, damit besser interpoliert werden kann
    x_klein=x[y<=schranke]
    y_gross=y[y>schranke]
    x_gross=x[y>schranke]
    ymin=min(y)
    ymax=max(y)
    xmin=min(x)
    xmax=max(x)
    xnew=np.concatenate((x_gross, x_klein))           #verbinde Daten zu neuen
    ynew=np.concatenate((y_gross, y_klein))
    
    x_pdf=stats.norm.pdf(xnew, mean_x, sd_x)          #f(X)
    
    eval_points=np.linspace(ymin, ymax, 1000)         #KDE von f(Y), dafür 1000 Stützstellen auswerten und interpolieren
    kde_Y = gaussian_kde(y, bw_method='silverman')    #KDE ausgerwertet komplett steht in y_grid
    Y_auswertung_1000 = kde_Y.pdf(eval_points)

    x_grid = ynew                                     #evaluate in y
    f = interp1d(eval_points, Y_auswertung_1000, kind="cubic", assume_sorted=True)
    y_grid = f(x_grid)

    plt.figure(1, dpi=800)                            #plot density estimation Y
    plt.hist(y,bins=80,density=True)
    plt.plot(eval_points,Y_auswertung_1000)
    plt.plot(ynew, y_grid,linestyle = "" , marker = ".", color='r', markersize='0.2' )
    plt.show()
    
    plt.hist(x,bins=80,density=True)                  #plot density estimation X
    plt.plot(xnew, x_pdf,linestyle = "" , marker = ".", color='r', markersize='0.2' )
    
    
    KDE_x_y, X,Y,Z, positions=interpolate2d(xmin, xmax, ymin, schranke, x, y, x_klein, y_klein, x_gross, y_gross)
        
    M=10000
    triang = mtri.Triangulation(xnew[:M], ynew[:M])

    fig = plt.figure(figsize=(13, 7))
    ax = fig.add_subplot( projection='3d')
    ax.plot_trisurf(triang, KDE_x_y[:M], cmap='jet')
    ax.scatter(xnew[:M], ynew[:M],KDE_x_y[:M], marker='.', s=10, c="black", alpha=0.5)
    ax.view_init(elev=60, azim=-45)

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    plt.show()
    
    test=y_grid*x_pdf/KDE_x_y

    test2=abs(test-1)

    return 1/L*sum(test2), test, X,Y,Z

#%% 
#small scale example

Nx=400  #Nx+1 grid points on x axis
Ny=200  #Ny+1 grid ponts on y achsis
t=50    #number of training parameters for MC and UMC, to change number of training parameters
        #for CC and GL change iLevel in subspaceV
M=100   #how many solutions should be predicted

u_a=0.4; u_b=20; erel_a=0.1; erel_b=0.9; mu_a=0.005 ; mu_b= 0.05; c_a= 10*10**(-6);  c_b=50*10**(-6)
mean = [10.05, 0.5, 0.0275, 30*10**(-6)]
cov = [[1.8**2, 0, 0, 0], [0, 0.072**2, 0, 0], [0, 0,  0.004**2, 0], [0, 0, 0, 0.000003**2]] 
A00, A0, A1, A2, A3, b = affine_dec(0, Nx, Ny, 0, 0.08, 0, 0.02, 0.08/math.pi)

N=13    #number of reduced basis functions

#compute transformation matrix V
C, WM,B, V, u_train, erel_train, mu_train, c_train, evtest, sumweights=subspaceV("Uniform Monte-Carlo", "newpapereig", N, Nx, Ny, t, 0,0,0,0)

#approximate solutions for new parameters and compare to true solutions
u, erel, mu, c = np.random.multivariate_normal(mean, cov, M).T
lsg=[aff_dec_hd(A00, A0, A1, A2, A3, b, u[i], erel[i], mu[i], c[i], Nx, Ny) for i in range(M)]

pred,  p_max, W, F=aff_dec_wPOD(np.transpose(V), A00, A0, A1, A2, A3, b, u, M, erel, c, mu, Nx, Ny)
for i in range(M):
    pred[i].shape=(((Ny+1)*(Nx+1)),)
    pred[i]=np.squeeze(np.asarray(pred[i]))
diff, diffv=euclidian_diff_app_error(lsg, pred, M, c,2," ")
print(diffv)

#f sens analysis for p_max, mu

f_mu_pmax, in_mu_p, X_mu_p, Y_mu_p, Z_mu_p=completefsensanalysis(mu, np.array(p_max), 25000000, 0.0275, 0.004)

x=mu
y=p_max
fig = plt.figure( dpi=800)
ax = fig.gca()
ax.set_xlim(min(x), max(x))
ax.set_ylim(min(y), 16000000)
plt.contourf(X_mu_p, Y_mu_p, Z_mu_p, 20, cmap='Blues')
ax.set_xlabel(r'$ \mu \ [m] $', fontsize=14)
ax.set_ylabel(r'$ p_max \ [Pa]$', fontsize=14)
ax.tick_params(axis='both', labelsize=12)
plt.colorbar();

